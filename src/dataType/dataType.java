package dataType;

public class dataType {
    public static void main(String[] args) {
//        String str = "Wawan";
//        byte num1 = 127;
//        short num2 = 32767;
//        int num3 = 2147483647;
//        long num4 = 9223372036854775807;
//        float num5 = 10000f;
//        double num6 = 0.122;
//        char num7 = '1';
//        char num8 = '%';
//        boolean num9 = false;
//        System.out.println((int) num2);

//        class
        dataType temp = new dataType();
        System.out.println(temp);

//        array
        String[] mhs1 = {"Wawan", "Yanto", "Thomas"};
        printArrStr1(mhs1);
        String[][] mhs2 = {
                {"Wawan", "111"},
                {"Yanto", "222"},
                {"Thomas", "333"}
        };
        printArrStr2(mhs2);
    }

    public static void printArrStr1(String[] x) {
        for (String temp : x) {
            System.out.println(temp);
        }
    }

    public static void printArrStr2(String[][] x) {
        for (String[] temp : x) {
            System.out.println(temp[0] + ", " + temp[1]);
//            for (int j = 0; j < temp.length; j++) {
//                System.out.println(temp[j]);
//            }
        }
    }
}
